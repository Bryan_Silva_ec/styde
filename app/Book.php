<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'name', 'description', 'author_id' , 'house_id',
    ];

    public function author() {
        $this->belongsTo(Author::class);
    }

    public function house() {
        $this->belongsTo(house::class);
    }
}

