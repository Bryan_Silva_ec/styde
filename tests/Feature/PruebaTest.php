<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PruebaTest extends TestCase
{
    /**
     * A basic test example.
     * vendor\bin\phpunit
     * @return void
     */
    public function test_load_page_prueba()
    {
        $this->get('/prueba')->assertStatus(200);
        //assertSee('texto que debe de mostrar la pagina')
    }
}
