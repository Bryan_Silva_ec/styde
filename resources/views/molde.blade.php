<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>@yield('title')</title>
</head>
<body>
    <header>
        <h1>@yield('encabezado')</h1>
    </header>

    <main>
        @yield('content')
    </main>


    <footer>
        <p> @yield('pie')</p>
    </footer>
</body>
</html>